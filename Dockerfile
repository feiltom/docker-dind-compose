FROM docker
RUN apk add --no-cache --virtual .build-deps gcc python3 libc-dev python3-dev libffi-dev openssl-dev make && python3 -m ensurepip && pip3 --no-cache-dir install --upgrade pip setuptools && pip --no-cache-dir install 'docker-compose==1.27.3' && apk del gcc libc-dev python3-dev libffi-dev openssl-dev make
RUN apk add --no-cache bash git
